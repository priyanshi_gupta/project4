const routes = [
  {
    path: "/",
    component: () => import("@/layouts/AuthLayout.vue"),
    beforeEnter: (to, from, next) => {
      const signedIn = localStorage.getItem("email");
      signedIn ? next("/my-bank") : next();
    },
    children: [
      {
        path: "",
        component: () => import("@/pages/signUp.vue")
      },
      {
        path: "/signUp",
        component: () => import("@/pages/signUp.vue")
      },
      {
        path: "/login",
        component: () => import("@/pages/login.vue")
      }
    ]
  },
  {
    path: "/main",
    component: () => import("@/layouts/MainLayout.vue"),
    beforeEnter: (to, from, next) => {
      const signedIn = localStorage.getItem("email");
      !signedIn ? next({ path: "/login" }) : next();
    },
    children: [
      {
        path: "/my-bank",
        component: () => import("@/pages/myBank.vue")
      },
      {
        path: "/passbook",
        component: () => import("@/pages/passbook.vue")
      },
      {
        path: "/tax",
        component: () => import("@/pages/tax.vue")
      }
    ]
  }
];

export default routes;
